#!/bin/sh -ex

apt -y -o Acquire::http::AllowRedirect=false update || true
apt -y -o Acquire::http::AllowRedirect=false upgrade --download-only || true

cd /var/cache/apt/archives

test -e apt-dbgsym_1.4.9_amd64.deb && \
    echo "1da507155c7b1ad140739c62fdacceaf5b5ee3765b1a00c3a3527d9d82a8d533  apt-dbgsym_1.4.9_amd64.deb" | sha256sum -c

test -e apt-transport-https-dbgsym_1.4.9_amd64.deb && \
    echo "59f3e1c91664fe3b47048794560ebe9c41f1eeccbdd95f7715282f8cbe449060  apt-transport-https-dbgsym_1.4.9_amd64.deb" | sha256sum -c

test -e apt-transport-https_1.4.9_amd64.deb && \
    echo "c8c4366d1912ff8223615891397a78b44f313b0a2f15a970a82abe48460490cb  apt-transport-https_1.4.9_amd64.deb" | sha256sum -c

test -e apt-utils-dbgsym_1.4.9_amd64.deb && \
    echo "e3e157c291b05b2899a545331c7597ab36ca04e02cd9010562b9985b76af60db  apt-utils-dbgsym_1.4.9_amd64.deb" | sha256sum -c

test -e apt-utils_1.4.9_amd64.deb && \
    echo "fb227d1c4615197a6263e7312851ac3601d946221cfd85f20427a15ab9658d15  apt-utils_1.4.9_amd64.deb" | sha256sum -c

test -e apt_1.4.9_amd64.deb && \
    echo "dddf4ff686845b82c6c778a70f1f607d0bb9f8aa43f2fb7983db4ff1a55f5fae  apt_1.4.9_amd64.deb" | sha256sum -c

test -e libapt-inst2.0-dbgsym_1.4.9_amd64.deb && \
    echo "0e66db1f74827f06c55ac36cc961e932cd0a9a6efab91b7d1159658bab5f533e  libapt-inst2.0-dbgsym_1.4.9_amd64.deb" | sha256sum -c

test -e libapt-inst2.0_1.4.9_amd64.deb && \
    echo "a099c57d20b3e55d224433b7a1ee972f6fdb79911322882d6e6f6a383862a57d  libapt-inst2.0_1.4.9_amd64.deb" | sha256sum -c

test -e libapt-pkg-dev_1.4.9_amd64.deb && \
    echo "cfb0a03ecd22aba066d97e75d4d00d791c7a3aceb2e5ec4fbee7176389717404  libapt-pkg-dev_1.4.9_amd64.deb" | sha256sum -c

test -e libapt-pkg5.0-dbgsym_1.4.9_amd64.deb && \
    echo "cdb03ddd57934e773a579a89f32f11567710a39d6ac289e73efb20e8825874d1  libapt-pkg5.0-dbgsym_1.4.9_amd64.deb" | sha256sum -c

test -e libapt-pkg5.0_1.4.9_amd64.deb && \
    echo "03281e3d1382826d5989c12c77a9b27f5f752b0f6aa28b524a2df193f7296e0b  libapt-pkg5.0_1.4.9_amd64.deb" | sha256sum -c

apt -y -o Acquire::http::AllowRedirect=false install --only-upgrade apt
